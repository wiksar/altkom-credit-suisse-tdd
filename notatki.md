
## GIT 
https://bitbucket.org/ev45ive/altkom-credit-suisse-tdd/src
Login
Klikamy (+) -> Fork this repository

git clone https://bitbucket.org/<TWOJ-LOGIN>/altkom-credit-suisse-tdd.git altkom-credit-suisse-tdd
cd altkom-credit-suisse-tdd

git remote add trener https://bitbucket.org/ev45ive/altkom-credit-suisse-tdd/

## Update
git pull trener master

git branch --set-upstream-to trener/master
git pull 


## Instalacje

node -v 
v16.13.1

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.64.2
f80445acd5a3dadef24aa209168452a3d97cc326

chrome://version
Google Chrome	98.0.4758.102 

## Konfiguracja